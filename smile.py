#!/usr/bin/env python3

import cv2
import time
from gi.repository import GObject
from gi.repository import Notify


def main():
    """
    Main function that captures video frame by frame, detects faces and smiles and sends notifications if no face or smile is detected for a certain period of time
    """
    # Initialize the Notification Object
    Notify.init("Resting Bitch Face Detection")

    # Send initial Notification
    notification = Notify.Notification.new("Resting Bitch Face", "Started Image Recognition Daemon")
    notification.show()

    # Load the Haar cascade files for detecting faces, eyes, and smiles
    try:
        face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades +'haarcascade_frontalface_default.xml')
        smile_cascade = cv2.CascadeClassifier(cv2.data.haarcascades +'haarcascade_smile.xml')
    except cv2.error as e:
        print("Error loading cascades: ", e)
        exit()


    # Define the detect function to detect faces and smiles in the image
    def detect(gray, frame):
        """
        Detects faces and smiles in an image, while limiting the ROI to the lower half of the face
        :param gray: grayscale image
        :param frame: original image
        :return: list of boolean values indicating if a face and a smile were detected
        """
        smiles = [""]
        # Detect faces in the image
        try:
            faces = face_cascade.detectMultiScale(gray, 1.3, 5)
        except cv2.error as e:
            print("Error detecting faces: ", e)
            return # Draw rectangles around the detected faces
        for (x, y, w, h) in faces:
            cv2.rectangle(frame, (x, y), ((x + w), (y + h)), (255, 0, 0), 2)
            # Create regions of interest (ROI) for the gray and color images
            roi_gray = gray[y:y + h, x:x + w]
            roi_color = frame[y:y + h, x:x + w]
            # Restrict ROI to lower half of the face
            roi_gray = roi_gray[int(h/2):h,:]
            roi_color = roi_color[int(h/2):h,:]
            # Detect smiles in the ROI
            try:
                smiles = smile_cascade.detectMultiScale(roi_gray, 2, 30)
            except cv2.error as e:
                print("Error detecting smiles: ", e)
                continue
            # Draw rectangles around the detected smiles
            for (sx, sy, sw, sh) in smiles:
                cv2.rectangle(roi_color, (sx, sy), ((sx + sw), (sy + sh)), (0, 0, 255), 2)

        detections = [len(faces) > 0, len(smiles) > 0]
        return detections


    # Create a VideoCapture object to access the camera
    try:
        video_capture = cv2.VideoCapture(0)
    except cv2.error as e:
        print("Error opening video capture: ", e)
        exit()

    # Timer to check for no face detection
    face_detected = True
    last_face_detection = time.time()
    last_smile_detection = time.time()

    # Enter the while loop to capture video frame by frame
    while video_capture.isOpened():
        # Read the current frame from the video capture
        _, frame = video_capture.read()

        # Convert the current frame to grayscale
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Call the detect function to detect faces and smiles in the image
        detections = detect(gray,frame)
        face_detected = detections[0]
        smile_detected = detections[1]

        # Check if no face was detected for 10 seconds
        if not face_detected:
            if (time.time() - last_face_detection) >= 10:
                print("No face detected for 10 seconds")
                notification = Notify.Notification.new("Resting Bitch Face", "No face detected for 10 seconds")
                notification.show()
                time.sleep(300)
        else:
            last_face_detection = time.time()
            if not smile_detected:
                if (time.time() - last_smile_detection) >= 10:
                    print("No smile detected for 10 seconds")
                    notification = Notify.Notification.new("Resting Bitch Face", "No smile detected for 10 seconds")
                    notification.show()
                    time.sleep(300)
            else:
                last_smile_detection = time.time()

        # Display the result on the camera feed
        #cv2.imshow('Video', frame)
        # Break the loop if the 'q' key is pressed
        #if cv2.waitKey(1) & 0xff == ord('q'):
        #    break

    # Release the video capture and destroy all windows once the processing is done
    video_capture.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
